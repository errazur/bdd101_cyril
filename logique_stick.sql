-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 15 juin 2021 à 09:12
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `logique_stick`
--

-- --------------------------------------------------------

--
-- Structure de la table `composition`
--

DROP TABLE IF EXISTS `composition`;
CREATE TABLE IF NOT EXISTS `composition` (
  `RefP` smallint(20) UNSIGNED NOT NULL,
  `RefM` smallint(6) DEFAULT NULL,
  `Quantite` double DEFAULT NULL,
  KEY `RefP` (`RefP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `composition`
--

INSERT INTO `composition` (`RefP`, `RefM`, `Quantite`) VALUES
(1, 13412, 150),
(2, 7, 80),
(3, 5, 1000),
(4, 13412, 1000),
(5, 3, 11);

-- --------------------------------------------------------

--
-- Structure de la table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
CREATE TABLE IF NOT EXISTS `fournisseur` (
  `idF` smallint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `NomF` varchar(40) DEFAULT NULL,
  `Adresse` text,
  PRIMARY KEY (`idF`),
  UNIQUE KEY `idF` (`idF`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fournisseur`
--

INSERT INTO `fournisseur` (`idF`, `NomF`, `Adresse`) VALUES
(1, 'LeGall', 'Chatelet'),
(2, 'Ferrailleur De Lextrem', 'Paris'),
(3, 'Fournisseur Man', 'Toulon'),
(4, 'José Fournit', 'Callac');

-- --------------------------------------------------------

--
-- Structure de la table `fournit`
--

DROP TABLE IF EXISTS `fournit`;
CREATE TABLE IF NOT EXISTS `fournit` (
  `RefM` smallint(20) UNSIGNED NOT NULL,
  `idF` smallint(20) UNSIGNED NOT NULL,
  KEY `RefM` (`RefM`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fournit`
--

INSERT INTO `fournit` (`RefM`, `idF`) VALUES
(13412, 4),
(2, 2),
(3, 3),
(5, 1),
(7, 1),
(4, 3),
(6, 4),
(4, 4);

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

DROP TABLE IF EXISTS `matiere`;
CREATE TABLE IF NOT EXISTS `matiere` (
  `RefM` smallint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `NomM` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`RefM`),
  UNIQUE KEY `RefM` (`RefM`)
) ENGINE=InnoDB AUTO_INCREMENT=13413 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`RefM`, `NomM`) VALUES
(2, 'NCD16'),
(3, 'PLA'),
(4, 'ABS'),
(5, 'NC19'),
(6, 'NC20'),
(7, 'MLX'),
(13412, 'DTM');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `RefP` smallint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Desc` text,
  PRIMARY KEY (`RefP`),
  UNIQUE KEY `RefP` (`RefP`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`RefP`, `Desc`) VALUES
(1, 'Vis de bielle'),
(2, 'Goujon'),
(3, 'Ecrou'),
(4, 'Booster'),
(5, 'Support telephone');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
