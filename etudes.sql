-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 15 juin 2021 à 09:12
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `etudes`
--

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

DROP TABLE IF EXISTS `cours`;
CREATE TABLE IF NOT EXISTS `cours` (
  `idC` int(11) NOT NULL AUTO_INCREMENT,
  `NomC` varchar(40) NOT NULL,
  `Jour` varchar(10) DEFAULT NULL,
  `HDeb` time DEFAULT NULL,
  `HFin` time DEFAULT NULL,
  PRIMARY KEY (`idC`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cours`
--

INSERT INTO `cours` (`idC`, `NomC`, `Jour`, `HDeb`, `HFin`) VALUES
(1, 'anglais', 'mardi', '09:00:00', '10:00:00'),
(2, 'français', 'lundi', '10:00:00', '12:00:00'),
(3, 'sciences', 'jeudi', '08:30:00', '10:00:00'),
(4, 'mathématiques', 'vendredi', '08:30:00', '10:30:00'),
(5, 'technologie', 'lundi', '10:00:00', '11:15:00'),
(6, 'mathématiques', 'mercredi', '08:00:00', '10:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `etudiants`
--

DROP TABLE IF EXISTS `etudiants`;
CREATE TABLE IF NOT EXISTS `etudiants` (
  `idE` int(11) NOT NULL AUTO_INCREMENT,
  `NomE` varchar(40) NOT NULL,
  `idS` smallint(11) DEFAULT NULL,
  PRIMARY KEY (`idE`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etudiants`
--

INSERT INTO `etudiants` (`idE`, `NomE`, `idS`) VALUES
(1, 'BOIREAU Maxime', 1),
(2, 'CAMPION Ugo', 2),
(3, 'HENRY Guillaume', 3),
(4, 'LE BACON Soazig', 4),
(5, 'LE DIRAISON Mathis', 5),
(6, 'LE NOIR Quentin', 4),
(7, 'MARAVAL Bastien', 1),
(8, 'TANGUY Gaël', 2),
(9, 'VALETTE Cyril', 3);

-- --------------------------------------------------------

--
-- Structure de la table `inscritcour`
--

DROP TABLE IF EXISTS `inscritcour`;
CREATE TABLE IF NOT EXISTS `inscritcour` (
  `idE` int(11) NOT NULL AUTO_INCREMENT,
  `idC` smallint(11) DEFAULT NULL,
  PRIMARY KEY (`idE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `inscritoption`
--

DROP TABLE IF EXISTS `inscritoption`;
CREATE TABLE IF NOT EXISTS `inscritoption` (
  `idE` int(11) NOT NULL AUTO_INCREMENT,
  `idO` smallint(11) DEFAULT NULL,
  PRIMARY KEY (`idE`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `inscritoption`
--

INSERT INTO `inscritoption` (`idE`, `idO`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 4),
(9, 3);

-- --------------------------------------------------------

--
-- Structure de la table `options`
--

DROP TABLE IF EXISTS `options`;
CREATE TABLE IF NOT EXISTS `options` (
  `idO` int(11) NOT NULL AUTO_INCREMENT,
  `NomO` varchar(40) NOT NULL,
  `Jour` varchar(10) DEFAULT NULL,
  `HDeb` time DEFAULT NULL,
  `HFin` time DEFAULT NULL,
  PRIMARY KEY (`idO`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `options`
--

INSERT INTO `options` (`idO`, `NomO`, `Jour`, `HDeb`, `HFin`) VALUES
(1, 'anglais perfectionnement', 'jeudi', '13:30:00', '14:30:00'),
(2, 'mathématiques intermédiaire', 'jeudi', '15:00:00', '16:30:00'),
(3, 'mathématiques expert', 'vendredi', '13:30:00', '15:30:00'),
(4, 'cinema audiovisuel', 'vendredi', '15:30:00', '17:30:00'),
(5, 'philosophie', 'mardi', '13:30:00', '14:45:00'),
(6, 'arts plastiques', 'mardi', '15:00:00', '16:30:00'),
(7, 'développement informatique', 'mercredi', '09:30:00', '11:30:00');

-- --------------------------------------------------------

--
-- Structure de la table `seminaires`
--

DROP TABLE IF EXISTS `seminaires`;
CREATE TABLE IF NOT EXISTS `seminaires` (
  `idS` int(11) NOT NULL AUTO_INCREMENT,
  `NomS` varchar(70) NOT NULL,
  `Jour` varchar(10) DEFAULT NULL,
  `HDeb` time DEFAULT NULL,
  `HFin` time DEFAULT NULL,
  PRIMARY KEY (`idS`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `seminaires`
--

INSERT INTO `seminaires` (`idS`, `NomS`, `Jour`, `HDeb`, `HFin`) VALUES
(1, 'Anglophonie dans le monde', 'lundi', '13:30:00', '14:30:00'),
(2, 'Les philosophes', 'lundi', '14:30:00', '15:30:00'),
(3, 'Sciences et industrie', 'lundi', '15:30:00', '16:30:00'),
(4, 'Géopolitique', 'vendredi', '16:30:00', '17:30:00'),
(5, 'Economie et social', 'mardi', '16:30:00', '17:30:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
