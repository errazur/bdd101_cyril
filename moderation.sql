-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 15 juin 2021 à 09:13
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `moderation`
--

-- --------------------------------------------------------

--
-- Structure de la table `cocktail`
--

DROP TABLE IF EXISTS `cocktail`;
CREATE TABLE IF NOT EXISTS `cocktail` (
  `idC` int(10) UNSIGNED NOT NULL,
  `NomC` text NOT NULL,
  `Prix` double NOT NULL,
  KEY `id` (`idC`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `cocktail`
--

INSERT INTO `cocktail` (`idC`, `NomC`, `Prix`) VALUES
(1, 'Mojito', 5),
(2, 'Daiquiri', 5),
(3, 'Pina Colada', 7),
(4, 'Capirinha', 8),
(5, 'Zombie', 4),
(6, 'Don Papa', 6.5),
(7, 'El Presidente', 8.3),
(8, 'Punch Cubain', 6.9),
(9, 'Angostura', 5.7),
(10, 'Mademoiselle', 7.2),
(11, 'Aloha', 5.8),
(12, 'Tropical Thunder', 9.2),
(13, 'Ti Punch', 4),
(14, 'Planteur', 4.3),
(15, 'Mai tai', 7.6),
(16, 'blue lagoon', 9.8),
(17, 'gin tonic', 7.5),
(18, 'gin fizz', 8.6);

-- --------------------------------------------------------

--
-- Structure de la table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
CREATE TABLE IF NOT EXISTS `ingredient` (
  `idI` int(10) UNSIGNED DEFAULT NULL,
  `NomI` text NOT NULL,
  `PrixU` double NOT NULL,
  KEY `id` (`idI`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ingredient`
--

INSERT INTO `ingredient` (`idI`, `NomI`, `PrixU`) VALUES
(1, 'Rhum blanc', 2),
(2, 'Rhum Brun', 2.5),
(3, 'sucre brun ', 0.3),
(4, 'citron vert', 0.5),
(5, 'eau gazeuse', 0.3),
(6, 'glace pilé', 0.1),
(7, 'jus ananas', 0.7),
(8, 'creme coco', 1.2),
(9, 'cachaca ', 2.3),
(10, 'jus pamplemousse', 0.7),
(11, 'sirop grenadine', 0.4),
(12, 'feuille de menthe', 0.3),
(13, 'liqueur orange', 1.7),
(14, 'vermouth', 0.9),
(15, 'jus d\'orange', 0.7),
(16, 'jus de pomme', 0.7),
(17, 'sirop d\'erable', 0.9),
(18, 'liqueur grenade', 1.7),
(19, 'jus fruit de la passion', 1.1),
(20, 'canelle', 0.4),
(21, 'sirop orgeat', 0.9),
(22, 'limonade', 1.2),
(23, 'angostura aromatic bitters', 1.6),
(24, 'bitter cerise', 1.9),
(25, 'liqueur de sureau', 0.9),
(26, 'jus de citron', 0.6),
(27, 'vodka', 2.5),
(28, 'curacao', 1.7),
(29, 'gin', 2.5),
(30, 'tonic', 1.2);

-- --------------------------------------------------------

--
-- Structure de la table `recettes`
--

DROP TABLE IF EXISTS `recettes`;
CREATE TABLE IF NOT EXISTS `recettes` (
  `idC` int(10) UNSIGNED NOT NULL,
  `idI` int(10) UNSIGNED NOT NULL,
  `Quantite` text,
  KEY `id` (`idC`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `recettes`
--

INSERT INTO `recettes` (`idC`, `idI`, `Quantite`) VALUES
(1, 1, '5 cl'),
(1, 12, '6 a 8 feuilles'),
(1, 4, '2 cl de jus'),
(2, 4, '2 cl '),
(2, 3, '2 CAC'),
(2, 1, '4 cl '),
(1, 5, NULL),
(1, 6, NULL),
(1, 3, '1 CAC'),
(3, 1, '4cl'),
(3, 2, '2cl'),
(3, 7, '12cl'),
(3, 8, '4cl'),
(13, 4, '1/2'),
(6, 3, '3CAC'),
(6, 2, '5cl'),
(4, 4, '1 entier'),
(4, 6, NULL),
(4, 3, '2CAC'),
(4, 9, '5cl'),
(5, 2, '3cl'),
(5, 1, '3cl'),
(5, 11, '1cl'),
(5, 10, '3cl'),
(13, 3, '1 CAC'),
(13, 1, '4 a 6 cl '),
(12, 19, '6cl'),
(12, 3, '3 CAC'),
(12, 1, '4cl'),
(11, 26, '1cl'),
(11, 8, '2 cl '),
(11, 15, '4cl'),
(11, 7, '4cl'),
(11, 2, '4cl'),
(10, 25, '1 cl'),
(10, 24, '2 trait'),
(10, 18, '2cl'),
(10, 1, '5cl'),
(9, 23, '3 gouttes'),
(9, 16, '3cl'),
(9, 17, '1cl'),
(9, 1, '4cl'),
(8, 11, '1 trait'),
(8, 7, '5cl'),
(8, 15, '5cl'),
(8, 2, '5cl'),
(7, 13, '1.5cl'),
(7, 14, '1cl'),
(7, 2, '5cl'),
(6, 4, '2 quartiers'),
(6, 12, '1 bouquet'),
(8, 22, NULL),
(14, 1, '6cl'),
(14, 15, '6cl'),
(14, 7, '6cl'),
(14, 4, '2cl'),
(14, 11, '1cl'),
(15, 1, '3cl'),
(15, 2, '3cl'),
(15, 21, '1.5cl'),
(15, 4, '1.5cl'),
(15, 13, '1cl'),
(16, 27, '4cl'),
(16, 28, '3cl'),
(16, 4, '2cl'),
(17, 29, '4cl'),
(17, 30, '8cl'),
(18, 29, '4cl'),
(18, 5, '8cl'),
(18, 4, '3cl'),
(18, 3, '1 CAC');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
