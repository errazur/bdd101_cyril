-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 15 juin 2021 à 09:12
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `compagnie_de_transport`
--

-- --------------------------------------------------------

--
-- Structure de la table `avion`
--

DROP TABLE IF EXISTS `avion`;
CREATE TABLE IF NOT EXISTS `avion` (
  `Immatriculation` int(10) UNSIGNED NOT NULL,
  `Type` varchar(40) NOT NULL,
  `Capacite` int(255) UNSIGNED NOT NULL,
  UNIQUE KEY `Immatriculation` (`Immatriculation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `avion`
--

INSERT INTO `avion` (`Immatriculation`, `Type`, `Capacite`) VALUES
(3, '737', 126),
(2, 'A321', 185),
(4, '737-MAX 7', 149),
(5, '737-800', 162),
(6, '737-MAX 8', 189),
(7, '737-900ER', 215),
(8, '767-300ER', 350),
(9, '777-200ER', 440);

-- --------------------------------------------------------

--
-- Structure de la table `liaison`
--

DROP TABLE IF EXISTS `liaison`;
CREATE TABLE IF NOT EXISTS `liaison` (
  `NoLi` int(10) UNSIGNED NOT NULL,
  `VilleDep` varchar(45) NOT NULL,
  `VilleArr` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `liaison`
--

INSERT INTO `liaison` (`NoLi`, `VilleDep`, `VilleArr`) VALUES
(100, 'Paris', 'Bruxelle'),
(101, 'Paris', 'Marseille'),
(102, 'Paris', 'Berlin'),
(103, 'Paris', 'New-York'),
(104, 'Paris', 'Tokyo'),
(105, 'Marseille', 'Barcelone');

-- --------------------------------------------------------

--
-- Structure de la table `vol`
--

DROP TABLE IF EXISTS `vol`;
CREATE TABLE IF NOT EXISTS `vol` (
  `NoVol` int(11) NOT NULL,
  `JDep` date NOT NULL,
  `JArr` date NOT NULL,
  `HDep` time NOT NULL,
  `HArr` time NOT NULL,
  `NoLi` int(10) UNSIGNED NOT NULL,
  `Immatriculation` int(10) UNSIGNED NOT NULL,
  KEY `NoLi` (`NoLi`,`Immatriculation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `vol`
--

INSERT INTO `vol` (`NoVol`, `JDep`, `JArr`, `HDep`, `HArr`, `NoLi`, `Immatriculation`) VALUES
(500, '2021-06-04', '2021-06-05', '20:30:00', '08:45:00', 100, 3),
(501, '2021-06-15', '2021-06-15', '15:45:10', '23:15:00', 103, 6),
(502, '2021-10-02', '2021-10-03', '23:25:30', '18:20:10', 104, 9),
(503, '2021-03-25', '2021-03-25', '12:00:30', '13:15:40', 105, 3),
(504, '2021-09-20', '2021-09-20', '11:00:10', '12:45:50', 102, 8),
(504, '2021-07-10', '2021-07-10', '07:00:30', '08:30:40', 101, 5),
(506, '2021-07-06', '2021-07-06', '08:30:30', '09:30:30', 100, 9);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
